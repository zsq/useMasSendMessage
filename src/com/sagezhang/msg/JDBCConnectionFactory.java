/**
 * 
 */
package com.sagezhang.msg;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author SageZhang E-mail:sssage@163.com
 * @version 创建时间：2017年3月21日  下午11:16:51
 */
public class JDBCConnectionFactory {
    private static ThreadLocal<Connection> threadLocal = new ThreadLocal<>();
    /**
     * mas数据库url&username&password
     * 由于mas的mysql版本为4.0，采用mysql-connector-java-5.0.2的jar包，亲测可行！！！！！
     */
    private static String url = "jdbc:mysql://00.00.00.000:3306/mas?zeroDateTimeBehavior=convertToNull&amp;useUnicode=true&amp;";//url不设置编码
    private static String username = "admin";
    private static String pwd = "admin";
    
//  private static Properties properties = new Properties();
//  static {
//      System.out.println("jbdc加载");
//      try {
//          properties.load(JDBCConnectionFactory.class
//                  .getResourceAsStream("msgjdbc.properties"));
//          Class.forName(properties.getProperty("driver"));
//
//      } catch (ClassNotFoundException e) {
//          e.printStackTrace();
//      } catch (FileNotFoundException e) {
//          e.printStackTrace();
//      } catch (IOException e) {
//          e.printStackTrace();
//      }
//  }

    
    public static Connection getConnection() {
        Connection connection = threadLocal.get();
        if (null == connection) {
            try {
//                connection = DriverManager.getConnection(
//                        properties.getProperty("url"),
//                        properties.getProperty("username"),
//                        properties.getProperty("password"));
//                threadLocal.set(connection);
                connection = DriverManager.getConnection(url, username, pwd);
                threadLocal.set(connection);
//                connection.createStatement().execute("SET NAMES 'latin1'");

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }
    
    

    public static void close() {
        Connection connection = threadLocal.get();
        if (null != connection) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
