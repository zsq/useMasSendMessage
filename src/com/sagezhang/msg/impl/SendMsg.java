/**
 * 
 */
package com.sagezhang.msg.impl;

/**
 * @author SageZhang E-mail:sssage@163.com
 * @version 创建时间：2017年3月21日  下午11:32:46
 */
public class SendMsg extends BaseDao {

    public void sendMessage(Object params[]) {
        System.out.println("往mas发送短信成功");
        /**
         * MOBILES：必填，手机号
         * CONTENT：必填，短信内容
         * 详见文档
         */
        String sql = "insert into api_mt_anquanpt(SM_ID,SRC_ID,MOBILES,CONTENT,IS_WAP,URL,SM_TYPE) values(0,0,?,?,0,'',0)";
        super.add(sql, params);

    }
}
