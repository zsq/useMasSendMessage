/**
 * 
 */
package com.sagezhang.msg.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.sagezhang.msg.IBaseDao;
import com.sagezhang.msg.JDBCConnectionFactory;

/**
 * @author SageZhang E-mail:sssage@163.com
 * @version 创建时间：2017年3月21日  下午11:30:34
 */
public class BaseDao implements IBaseDao {



    Connection connection = JDBCConnectionFactory.getConnection();

    @Override
    public boolean add(String sql, Object[] params) {

        int j = 0;
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                preparedStatement.setObject(i + 1, params[i]);
            }
            j = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return j == 0 ? false : true;
    }
}
